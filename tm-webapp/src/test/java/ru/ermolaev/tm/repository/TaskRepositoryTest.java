package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.ermolaev.tm.config.WebMvcConfiguration;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.entity.Task;
import ru.ermolaev.tm.entity.User;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebMvcConfiguration.class)
public class TaskRepositoryTest {

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private IUserRepository userRepository;

    private final User userA = new User();

    private final User userB = new User();

    private final Project projectA = new Project();

    private final Project projectB = new Project();

    private final Task taskA = new Task();

    private final Task taskB = new Task();

    @Before
    @Transactional
    public void prepareData() {
        userA.setLogin("admin");
        userRepository.save(userA);
        userB.setLogin("test");
        userRepository.save(userB);

        projectA.setName("admin-project");
        projectA.setUser(userA);
        projectRepository.save(projectA);

        projectB.setName("test-project");
        projectB.setUser(userB);
        projectRepository.save(projectB);

        taskA.setName("admin-task");
        taskA.setProject(projectA);
        taskA.setUser(userA);
        taskRepository.save(taskA);

        taskB.setName("test-task");
        taskB.setProject(projectB);
        taskB.setUser(userB);
        taskRepository.save(taskB);
    }

    @After
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
        projectRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void countByUserId() {
        Assert.assertEquals(2, taskRepository.count());
        Assert.assertEquals(1, taskRepository.countByUserId(userA.getId()));
        Assert.assertEquals(1, taskRepository.countByUserId(userB.getId()));
        Assert.assertEquals(0, taskRepository.countByUserId("wrong id"));
    }

    @Test
    public void countByProjectId() {
        Assert.assertEquals(2, taskRepository.count());
        Assert.assertEquals(1, taskRepository.countByProjectId(projectA.getId()));
        Assert.assertEquals(1, taskRepository.countByProjectId(projectB.getId()));
        Assert.assertEquals(0, taskRepository.countByProjectId("wrong id"));
    }

    @Test
    public void countByUserIdAndProjectId() {
        Assert.assertEquals(0, taskRepository.countByUserIdAndProjectId(userA.getId(), projectB.getId()));
        Assert.assertEquals(0, taskRepository.countByUserIdAndProjectId(userB.getId(), projectA.getId()));
        Assert.assertEquals(1, taskRepository.countByUserIdAndProjectId(userA.getId(), projectA.getId()));
        Assert.assertEquals(1, taskRepository.countByUserIdAndProjectId(userB.getId(), projectB.getId()));
    }

    @Test
    public void findByUserIdAndId() {
        @Nullable final Task task = taskRepository.findByUserIdAndId(userA.getId(), taskA.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), taskA.getId());
    }

    @Test
    public void findByUserIdAndName() {
        @Nullable final Task task = taskRepository.findByUserIdAndName(userA.getId(), taskA.getName());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), taskA.getId());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(2, taskRepository.findAll().size());
        for (int i = 0; i < 5; i++) {
            Task task = new Task();
            task.setUser(userA);
            taskRepository.save(task);
        }
        Assert.assertEquals(6, taskRepository.findAllByUserId(userA.getId()).size());
        Assert.assertEquals(1, taskRepository.findAllByUserId(userB.getId()).size());
    }

    @Test
    public void findAllByProjectId() {
        Assert.assertEquals(2, taskRepository.findAll().size());
        for (int i = 0; i < 5; i++) {
            Task task = new Task();
            task.setProject(projectA);
            taskRepository.save(task);
        }
        Assert.assertEquals(6, taskRepository.findAllByProjectId(projectA.getId()).size());
        Assert.assertEquals(1, taskRepository.findAllByProjectId(projectB.getId()).size());
    }

    @Test
    public void findAllByUserIdAndProjectId() {
        for (int i = 0; i < 5; i++) {
            Task task = new Task();
            task.setUser(userA);
            task.setProject(projectA);
            taskRepository.save(task);
        }
        Assert.assertEquals(6, taskRepository.findAllByUserIdAndProjectId(userA.getId(), projectA.getId()).size());
        Assert.assertEquals(1, taskRepository.findAllByUserIdAndProjectId(userB.getId(), projectB.getId()).size());
        Assert.assertEquals(0, taskRepository.findAllByUserIdAndProjectId(userA.getId(), projectB.getId()).size());
        Assert.assertEquals(0, taskRepository.findAllByUserIdAndProjectId(userB.getId(), projectA.getId()).size());
    }

    @Test
    @Transactional
    public void deleteByUserIdAndId() {
        Assert.assertEquals(2, taskRepository.findAll().size());
        taskRepository.deleteByUserIdAndId(userA.getId(), taskA.getId());
        Assert.assertEquals(1, taskRepository.findAll().size());
        @Nullable final Task task = taskRepository.findById(taskA.getId()).orElse(null);
        Assert.assertNull(task);
    }

    @Test
    @Transactional
    public void deleteByUserIdAndName() {
        Assert.assertEquals(2, taskRepository.findAll().size());
        taskRepository.deleteByUserIdAndName(userA.getId(), taskA.getName());
        Assert.assertEquals(1, taskRepository.findAll().size());
        @Nullable final Task task = taskRepository.findById(taskA.getId()).orElse(null);
        Assert.assertNull(task);
    }

    @Test
    @Transactional
    public void deleteAllByUserId() {
        Assert.assertEquals(2, taskRepository.findAll().size());
        for (int i = 0; i < 5; i++) {
            Task task = new Task();
            task.setUser(userA);
            taskRepository.save(task);
        }
        Assert.assertEquals(7, taskRepository.findAll().size());
        taskRepository.deleteAllByUserId(userA.getId());
        Assert.assertEquals(1, taskRepository.findAll().size());
    }

    @Test
    @Transactional
    public void deleteAllByProjectId() {
        Assert.assertEquals(2, taskRepository.findAll().size());
        for (int i = 0; i < 5; i++) {
            Task task = new Task();
            task.setProject(projectA);
            taskRepository.save(task);
        }
        Assert.assertEquals(7, taskRepository.findAll().size());
        taskRepository.deleteAllByProjectId(projectA.getId());
        Assert.assertEquals(1, taskRepository.findAll().size());
    }

    @Test
    @Transactional
    public void deleteAllByUserIdAndProjectId() {
        Assert.assertEquals(2, taskRepository.findAll().size());
        for (int i = 0; i < 5; i++) {
            Task task = new Task();
            task.setUser(userA);
            task.setProject(projectA);
            taskRepository.save(task);
        }
        Assert.assertEquals(7, taskRepository.findAll().size());
        taskRepository.deleteAllByUserIdAndProjectId(userA.getId(), projectA.getId());
        Assert.assertEquals(1, taskRepository.findAll().size());
    }

}
