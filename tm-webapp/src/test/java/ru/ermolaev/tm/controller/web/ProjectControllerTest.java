package ru.ermolaev.tm.controller.web;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.ermolaev.tm.controller.AbstractControllerTest;
import ru.ermolaev.tm.dto.ProjectDTO;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProjectControllerTest extends AbstractControllerTest {

    private final String baseUrl = "/projects";

    @Test
    public void showTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/show"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/project/projectList"));
    }

    @Test
    public void viewTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/view/{id}", projectA.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/project/projectView"));
    }

    @Test
    public void createGetTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/create"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/project/projectCreate"));
    }

    @Test
    public void createPostTest() throws Exception {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("test");
        projectDTO.setDescription("test");
        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/create").flashAttr("project", projectDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/projects/show"))
                .andExpect(view().name("redirect:/projects/show"));
    }

    @Test
    public void removeTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/remove/{id}", projectA.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/projects/show"))
                .andExpect(view().name("redirect:/projects/show"));
    }

    @Test
    public void editGetTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/update/{id}", projectA.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/project/projectUpdate"));
    }

    @Test
    public void editPostTest() throws Exception {
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName("newName");
        projectDTO.setDescription("newDesc");
        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/update/{id}", projectA.getId()).flashAttr("project", projectDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/projects/show"))
                .andExpect(view().name("redirect:/projects/show"));
    }

}
