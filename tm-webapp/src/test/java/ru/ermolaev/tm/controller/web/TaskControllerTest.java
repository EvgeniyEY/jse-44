package ru.ermolaev.tm.controller.web;

import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.ermolaev.tm.controller.AbstractControllerTest;
import ru.ermolaev.tm.dto.TaskDTO;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

public class TaskControllerTest extends AbstractControllerTest {

    private final String baseUrl = "/tasks";

    @Test
    public void showTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/show"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/task/taskList"));
    }

    @Test
    public void viewTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/view/{id}", taskA.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/task/taskView"));
    }

    @Test
    public void createGetTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/create"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/task/taskCreate"));
    }

    @Test
    public void createPostTest() throws Exception {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName("test");
        taskDTO.setDescription("test");
        taskDTO.setProjectId(projectA.getId());
        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/create").flashAttr("task", taskDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/tasks/show"))
                .andExpect(view().name("redirect:/tasks/show"));
    }

    @Test
    public void removeTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/remove/{id}", taskA.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/tasks/show"))
                .andExpect(view().name("redirect:/tasks/show"));
    }

    @Test
    public void editGetTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/update/{id}", taskA.getId()))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("/task/taskUpdate"));
    }

    @Test
    public void editPostTest() throws Exception {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName("newName");
        taskDTO.setDescription("newDesc");
        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/update/{id}", taskA.getId()).flashAttr("task", taskDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/tasks/show"))
                .andExpect(view().name("redirect:/tasks/show"));
    }
}
