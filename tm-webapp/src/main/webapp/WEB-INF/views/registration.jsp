<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="resources/_prePage.jsp" />

<h2>Registration</h2>
<h3>Enter your credentials</h3>
<form:form action='/users/registration' method='POST' modelAttribute="user">
    <table>
        <tr>
            <td>Username:</td>
            <td><input type='text' name='login' value=''/></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type='password' name='passwordHash'/></td>
        </tr>
        <tr>
            <td>E-mail:</td>
            <td><input type='email' name='email'/></td>
        </tr>
        <tr>
            <td colspan='2'><input class="button" type="submit" value="Create account"/></td>
        </tr>
    </table>
</form:form>

<jsp:include page="resources/_postPage.jsp" />
