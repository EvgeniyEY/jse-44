package ru.ermolaev.tm.exception.unknown;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.exception.AbstractException;

public final class UnknownIndexException extends AbstractException {

    public UnknownIndexException() {
        super("Error! This index does not exist.");
    }

    public UnknownIndexException(@NotNull final Integer index) {
        super("Error! This index [" + index + "] does not exist.");
    }

}
