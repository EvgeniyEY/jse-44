package ru.ermolaev.tm.exception.user;

import ru.ermolaev.tm.exception.AbstractException;

public final class AccessLockedException extends AbstractException {

    public AccessLockedException() {
        super("Error! Your account is locked.");
    }

}
