package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.ProjectDTO;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.exception.empty.*;
import ru.ermolaev.tm.exception.incorrect.IncorrectCompleteDateException;
import ru.ermolaev.tm.exception.incorrect.IncorrectStartDateException;
import ru.ermolaev.tm.repository.IProjectRepository;

import java.util.Date;
import java.util.List;

@Service
public class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IUserService userService;

    private final IProjectRepository projectRepository;

    @Autowired
    public ProjectService(
            @NotNull final IUserService userService,
            @NotNull final IProjectRepository projectRepository
    ) {
        this.userService = userService;
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public Project getOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectRepository.getOne(id);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public Project createProject(
            @Nullable final String userId,
            @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        if (userId == null) return null;
        if (projectDTO == null) return null;
        @NotNull final Project project = new Project();
        if (projectDTO.getName() == null || projectDTO.getName().isEmpty()) throw new EmptyNameException();
        project.setName(projectDTO.getName());
        if (projectDTO.getDescription() == null || projectDTO.getDescription().isEmpty()) throw new EmptyNameException();
        project.setDescription(projectDTO.getDescription());
        project.setUser(userService.getOneById(userId));
        projectRepository.save(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public Project updateById(
            @Nullable final String userId,
            @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        if (userId == null) return null;
        if (projectDTO == null) return null;
        @Nullable final String id = projectDTO.getId();
        @Nullable final String name = projectDTO.getName();
        @Nullable final String description = projectDTO.getDescription();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null) throw new EmptyNameException();
        if (description == null) throw new EmptyDescriptionException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return null;
        if (!name.isEmpty()) project.setName(name);
        if (!description.isEmpty()) project.setDescription(description);
        if (name.isEmpty() && description.isEmpty()) return null;
        projectRepository.save(project);
        return project;
    }

    @Override
    @Transactional
    public void updateStartDate(
            @Nullable final String userId,
            @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        if (userId == null) return;
        if (projectDTO == null) return;
        @Nullable final String id = projectDTO.getId();
        @Nullable final Date date = projectDTO.getStartDate();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return;
        if (date.before(new Date(System.currentTimeMillis()))) throw new IncorrectStartDateException(date);
        project.setStartDate(date);
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void updateCompleteDate(
            @Nullable final String userId,
            @Nullable final ProjectDTO projectDTO
    ) throws Exception {
        if (userId == null) return;
        if (projectDTO == null) return;
        @Nullable final String id = projectDTO.getId();
        @Nullable final Date date = projectDTO.getCompleteDate();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) return;
        if (project.getStartDate() == null) throw new IncorrectCompleteDateException(date);
        if (project.getStartDate().after(date)) throw new IncorrectCompleteDateException(date);
        project.setCompleteDate(date);
        projectRepository.save(project);
    }

    @NotNull
    @Override
    public Long countAllProjects() {
        return projectRepository.count();
    }

    @NotNull
    @Override
    public Long countUserProjects(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.countByUserId(userId);
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public Project findOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByUserIdAndName(userId, name);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        return ProjectDTO.toDTO(projectRepository.findAll());
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return ProjectDTO.toDTO(projectRepository.findAllByUserId(userId));
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        projectRepository.deleteByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public void removeAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.deleteAllByUserId(userId);
    }

}
