package ru.ermolaev.tm.controller.view;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.dto.AuthUser;
import ru.ermolaev.tm.dto.TaskDTO;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    private final ITaskService taskService;

    private final IProjectService projectService;

    @Autowired
    public TaskController(
            @NotNull final ITaskService taskService,
            @NotNull final IProjectService projectService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @GetMapping("/show")
    public ModelAndView show(
            @AuthenticationPrincipal final AuthUser user
    ) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/taskList");
        modelAndView.addObject("tasks", taskService.findAllByUserId(user.getUserId()));
        return modelAndView;
    }

    @GetMapping("/view/{id}")
    public ModelAndView view(
            @PathVariable("id") @NotNull final String id,
            @AuthenticationPrincipal final AuthUser user
    ) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/taskView");
        @Nullable final TaskDTO taskDTO = TaskDTO.toDTO(taskService.findOneById(user.getUserId(), id));
        if (taskDTO == null) return null;
        modelAndView.addObject("task", taskDTO);
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create(
            @AuthenticationPrincipal final AuthUser user
    ) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/taskCreate");
        modelAndView.addObject("projects", projectService.findAllByUserId(user.getUserId()));
        return modelAndView;
    }

    @PostMapping("/create")
    public ModelAndView create(
            @ModelAttribute("task") @NotNull final TaskDTO taskDTO,
            @AuthenticationPrincipal final AuthUser user
    ) throws Exception {
        taskService.createTask(user.getUserId(), taskDTO);
        return new ModelAndView("redirect:/tasks/show");
    }

    @GetMapping("/remove/{id}")
    public ModelAndView remove(
            @PathVariable(value = "id") @NotNull final String id,
            @AuthenticationPrincipal final AuthUser user
    ) throws Exception {
        taskService.removeOneById(user.getUserId(), id);
        return new ModelAndView("redirect:/tasks/show");
    }

    @GetMapping("/update/{id}")
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id,
            @AuthenticationPrincipal final AuthUser user
    ) throws Exception {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/task/taskUpdate");
        @Nullable final TaskDTO taskDTO = TaskDTO.toDTO(taskService.findOneById(user.getUserId(), id));
        if (taskDTO == null) return null;
        modelAndView.addObject("task", taskDTO);
        modelAndView.addObject("projects", projectService.findAllByUserId(user.getUserId()));
        return modelAndView;
    }

    @PostMapping("/update/{id}")
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id,
            @ModelAttribute("task") @NotNull final TaskDTO taskDTO,
            @AuthenticationPrincipal final AuthUser user
    ) throws Exception {
        taskDTO.setId(id);
        taskService.updateById(user.getUserId(), taskDTO);
        return new ModelAndView("redirect:/tasks/show");
    }

}