package ru.ermolaev.tm.controller.view;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.UserDTO;

@Controller
@RequestMapping("/users")
public class UserController {

    private final IUserService userService;

    @Autowired
    public UserController(
            @NotNull final IUserService userService
    ) {
        this.userService = userService;
    }

    @GetMapping("/registration")
    public ModelAndView create() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/registration");
        return modelAndView;
    }

    @PostMapping("/registration")
    public ModelAndView create(
            @ModelAttribute("user") @NotNull final UserDTO userDTO
    ) throws Exception {
        userService.create(userDTO.getLogin(), userDTO.getPasswordHash(), userDTO.getEmail());
        return new ModelAndView("redirect:/");
    }

}
