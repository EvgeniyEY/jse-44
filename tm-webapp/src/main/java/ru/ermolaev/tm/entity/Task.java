package ru.ermolaev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_task")
public class Task extends AbstractEntity {

    @Column(nullable = false)
    private String name = "";

    @Column
    private String description = "";

    private Date startDate;

    private Date completeDate;

    @Column(updatable = false)
    private Date creationDate = new Date(System.currentTimeMillis());

    @ManyToOne
    private Project project;

    @ManyToOne
    private User user;

}
