package ru.ermolaev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.enumeration.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_user")
public class User extends AbstractEntity {

    @Column(unique = true,
            nullable = false,
            updatable = false)
    private String login = "";

    @Column(nullable = false)
    private String passwordHash = "";

    @Column
    private String email = "";

    @Column
    private String firstName = "";

    @Column
    private String middleName = "";

    @Column
    private String lastName = "";

    @Enumerated(value = EnumType.STRING)
    private Role role = Role.USER;

    @Column
    private Boolean locked = false;

    @NotNull
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

}
