package io.swagger.tmclient;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import io.swagger.tmclient.bootstrap.Bootstrap;
import io.swagger.tmclient.config.ClientConfiguration;

public class Client {

    public static void main(@Nullable final String[] args) {
        @NotNull final AnnotationConfigApplicationContext clientContext =
                new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = clientContext.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}
