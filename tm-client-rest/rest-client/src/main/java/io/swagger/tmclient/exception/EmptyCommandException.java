package io.swagger.tmclient.exception;

public class EmptyCommandException extends AbstractException {

    public EmptyCommandException() {
        super("Error! Command is empty.");
    }

}
