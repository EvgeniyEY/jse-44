package io.swagger.tmclient.command;

import io.swagger.client.api.DefaultApi;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractCommand {

    protected final DefaultApi api;

    @Autowired
    public AbstractCommand(@NotNull final DefaultApi api) {
        this.api = api;
    }

    @Nullable
    public abstract String commandName();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute() throws Exception;

}
