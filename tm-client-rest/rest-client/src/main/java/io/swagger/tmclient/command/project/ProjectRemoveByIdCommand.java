package io.swagger.tmclient.command.project;

import io.swagger.client.api.DefaultApi;
import io.swagger.tmclient.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import io.swagger.tmclient.util.TerminalUtil;

@Component
public class ProjectRemoveByIdCommand extends AbstractCommand {

    @Autowired
    public ProjectRemoveByIdCommand(@NotNull final DefaultApi api) {
        super(api);
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        api.removeById(id);
        System.out.println("[COMPLETE]");
    }

}
