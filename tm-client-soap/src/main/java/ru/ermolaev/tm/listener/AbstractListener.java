package ru.ermolaev.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.service.SessionService;

public abstract class AbstractListener {

    protected SessionService sessionService;

    @Autowired
    public AbstractListener(@NotNull final SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Nullable
    public abstract String command();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void handler(final ConsoleEvent event) throws Exception;

}
